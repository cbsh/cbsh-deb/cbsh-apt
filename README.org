#+TITLE: cbsh-apt
#+AUTHOR: Cameron Miller

The Third Party Apt Repository for CBSH, built with Debian Sid (Unstable) in mind, i will try my best to make sure it works with Testing and Stable, but no promises there.

Current Packages in Repository:
 - alacritty-cbsh -- Alacritty Config for CBSH
 - cbsh-sddm-theme -- The CBSH SDDM Theme
 - dmenu-cbsh -- My patched build of dmenu for CBSH
 - dmenu-scripts -- A collection of dmenu scripts to help with some tasks
 - doom-cbsh -- My Doom Emacs config
 - kitty-cbsh -- A kitty config and theme for CBSH
 - qtile-cbsh -- The main Qtile config for CBSH
 - st-cbsh -- My patched build of the Suckless Terminal (st)
 - zsh-cbsh -- My .zshrc config for zsh
